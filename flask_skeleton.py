# Pages to test: https://console.actions.google.com/u/1/project/codelab-example-a6683/simulator

# This program gets every message send through the application codelab-example (in console.actions.google.com under
# the user of federicocapdeville@gmail.com).
# And generates 2 webhooks:
#   First over the "Main Invocation": flask obtain variable over the name 'main_invocation'. It can be seen in
#                                     "Message received".
#                                     Also, flask_skeleton.py sends a string that can be audible using Google Actions.
#                                     It string is: "Message send through the Edge Device."
#   Second over the "Start Scene": Edge device send an integer that can be audible using Google Actions.
#                                  It is auto-incremental.

"""
Pages with important information:

Functionality of gnrok: https://www.youtube.com/watch?v=YMBzb_RBDAA
Functionality of google actions (Codelab1 and Codelab2):
https://www.youtube.com/watch?v=nVbwk4UKHWw&list=PLG9FQRMgm_JIF-ABw45PjuXbdv4unKk_X&index=3
https://www.youtube.com/watch?v=I7p6A-GI6E8&list=PLG9FQRMgm_JIF-ABw45PjuXbdv4unKk_X&index=8

Documentation_Sphinx of webhooks:
https://developers.google.com/assistant/conversational/webhooks#response-json_1
"""

"""
First run gnrok in 
/home/fcapdeville/Temporal/Programas/ngrok
with the command:
./ngrok http 5000

Then copy the Forwarding with https
https://45bc5cc15ab3.ngrok.io and paste it to the test application in Google Actions page:
https://console.actions.google.com/u/1/project/codelab-example-a6683/fulfillments/
As an example of the gnrok response is:
https://45bc5cc15ab3.ngrok.io/webhook
"""

from flask import Flask, request, Response
import json

app = Flask(__name__)

my_counter = 1


@app.route('/')
def api_root():
    return "Hello People"


@app.route('/webhook', methods=['POST'])
def webhook():
    global my_counter

    req = request.json
    print('Message received: ' + req['handler']['name'])
    print('Complete message: ' + str(req))

    response = {
        "session": {
            "id": "session_id",
            "params": {}
        },
        "prompt": {
            "override": False,
            "firstSimple": {
                "speech": "Message send through the Edge Device.",
            }
        },
        "user": {
            "locale": "en-US",
            "params": {
                "counter": my_counter
            }
        },
    }
    response['session']['id'] = req['session']['id']
    response['user']['params']['counter'] = my_counter
    print('counter is: ' + str(my_counter))

    my_counter += 1
    return Response(status=200, response=json.dumps(response))


if __name__ == '__main__':
    app.run(debug=True)
