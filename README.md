# Introduction: #
This is the project associated with the Google Actions research for the Edge Device.
It will generate tests for the functionalities described in the next link:

-----

### Files and Resources needed: ###
flask_skeleton.py allows us to create a localhost server which communicates to the Google Actions develop generated in the next page:

https://console.actions.google.com/u/1/project/codelab-example-a6683/

It is important to observe that every Google Actions development is attached to a specific gmail account.
This allows us to perform GET or POST depending on the necessities (sending or getting information to/from the Google Actions performance).


It will need a gnrok server created. It can be downloaded form its page:

https://ngrok.com/

After this, commands needed are:

`./ngrok http <PORT_TO_USE>`

As an example:

`./ngrok http 5000`

We need to copy the https URL generated over the webhook association in the fulfillments of Google Actions project. As an example, it can be found over:

https://console.actions.google.com/u/1/project/codelab-example-a6683/fulfillments/


----

### Resources required in Python: ###
- flask
- json

---

# How to run: #

- Perform ngrok commands, copy the URL to the Google Actions development.
- Run flask_skeleton.py
- Run Google Actions Tests.
- Every message send to the application can be seen locally in Python's terminal.

----

### Research and further information: ###

Functionality of gnrok:

https://www.youtube.com/watch?v=YMBzb_RBDAA


Functionality of google actions (Codelab1 and Codelab2):

https://www.youtube.com/watch?v=nVbwk4UKHWw&list=PLG9FQRMgm_JIF-ABw45PjuXbdv4unKk_X&index=3

https://www.youtube.com/watch?v=I7p6A-GI6E8&list=PLG9FQRMgm_JIF-ABw45PjuXbdv4unKk_X&index=8

Documentation of webhooks:

https://developers.google.com/assistant/conversational/webhooks#response-json_1
